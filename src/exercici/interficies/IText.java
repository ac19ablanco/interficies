package exercici.interficies;

public interface IText extends IColor {

    default String modificarColorFont(String codiColor, String text) {
        return codiColor + text;
    }

    default String modificarColorFons(String codiColor, String text) {
        return codiColor + text;
    }

    default String restablirConsola() {
        return ANSI_RESET;
    }
    abstract public String treureEspais(String text);

    abstract public String invertir(String text);
}
