package exercici.interficies;

public class Text implements IText {

    @Override
    public String treureEspais(String text) {
        return text.replaceAll(" ","");
    }

    @Override
    public String invertir(String text) {
        StringBuilder Stbd = new StringBuilder(text);
        return Stbd.reverse().toString();
    }

    public int cmpt(String text) {
        return (text).length();
    }
}
