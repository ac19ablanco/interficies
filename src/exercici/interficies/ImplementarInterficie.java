package exercici.interficies;

import java.awt.Color;
import java.util.Random;

public class ImplementarInterficie {

    static String imprimirColorRandom(IText itext, String text) {

        int random = (int) (Math.random() * 8);
        String colorRandom = "\u001B[4" + random + "m";

        return itext.modificarColorFons(colorRandom, text);
    }

    public static void main(String[] args) {
        IText text = new Text();
        String frase = "Hola que tal";

        System.out.println("Canviar color de fons: \n"
        + text.modificarColorFons(IColor.PURPLE_BACKGROUND, frase));
        
        
        System.out.println("Canviar color : \n"
        + text.modificarColorFons(IColor.CYAN, frase) + text.restablirConsola());
        
        
        System.out.println("Contar lletres sense espais : \n"
        + Text.class.cast(text).cmpt(text.treureEspais("Una frase \"Hola que tal")));
        

        System.out.println("Contar lletres: \n"
        + Text.class.cast(text).cmpt(frase));


        System.out.println(text.restablirConsola()+"Random: \n"
        + imprimirColorRandom(text, frase));
        
        System.out.println("Treure els espais : \n"
        + text.treureEspais(frase));
    }
}
